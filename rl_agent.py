import kuimaze
import numpy as np
import random
import os
import time

# constants
# MAP = 'maps/normal/normal3.bmp'
MAP = 'maps/easy/easy2.bmp'
MAP = os.path.join(os.path.dirname(os.path.abspath(__file__)), MAP)
# PROBS = [0.8, 0.1, 0.1, 0]
PROBS = [1, 0, 0, 0]
GRAD = (0, 0)
MAX_LEARNING_TIME = 19

def get_visualisation(table):
	ret = []
	for i in range(len(table[0])):
		for j in range(len(table)):
			ret.append({'x': j, 'y': i, 'value': [table[j][i][0], table[j][i][1], table[j][i][2], table[j][i][3]]})
	return ret

def choose_action(env, state, q_table, epsilon):
	# pick best action from q-table
	if random.random() < epsilon:
		indexes_of_best_actions = np.where(q_table[state[0], state[1], :] == max(q_table[state[0], state[1], :]))
		return indexes_of_best_actions[0][random.randint(0, len(indexes_of_best_actions[0]) - 1)] # if there are more values of the same type, pick one at random

	# pick random action
	return env.action_space.sample()


def update_q_table(q_table, action, reward, state, new_state, alpha):
	current_q_value = q_table[state[0], state[1], action]
	best_q_value_in_new_state = max(q_table[new_state[0], new_state[1], :])

	updateq_q_value = current_q_value + alpha * (reward + best_q_value_in_new_state - current_q_value)
	q_table[state[0], state[1], action] = updateq_q_value


def adjust_learning_params(alpha, epsilon, elapsed_time):
	"""Adjust learning parameters."""

	if elapsed_time < MAX_LEARNING_TIME / 4:
		return alpha, epsilon
	if elapsed_time < MAX_LEARNING_TIME / 2:
		return 0.6, 0.4
	if elapsed_time < MAX_LEARNING_TIME * 3 / 4:
		return 0.3, 0.6
	return 0.1, 0.8


def learn_policy(env):
	time_of_start = time.time()

	# Maze size
	x_dims = env.observation_space.spaces[0].n
	y_dims = env.observation_space.spaces[1].n
	maze_size = tuple((x_dims, y_dims))

	# Number of discrete actions
	num_actions = env.action_space.n

	# Q-table:
	q_table = np.zeros([maze_size[0], maze_size[1], num_actions], dtype=float)

	# learning rate
	alpha = 0.9

	# chance to pick learned action
	epsilon = 0.2

	while time.time() - time_of_start < MAX_LEARNING_TIME:
		# after each episode reset agents position
		is_done = False
		observation = env.reset()
		state = observation[0:2]

		#adjust learning rate and chance to pick best action
		alpha, epsilon = adjust_learning_params(alpha, epsilon, elapsed_time=time.time() - time_of_start)

		while not is_done:
			action = choose_action(env, state, q_table, epsilon)

			# take action
			observation, reward, is_done, _ = env.step(action)
			new_state = observation[0:2]
			update_q_table(q_table, action, reward, state, new_state, alpha)

			state = new_state

	# retrieve policy from q-table
	policy = {}
	for row in range (x_dims):
		for col in range(y_dims):
			index_of_best_actions = np.where(q_table[row, col, :] == max(q_table[row, col, :]))
			policy[(row, col)] = index_of_best_actions[0][0]

	return policy




if __name__ == "__main__":
	# Initialize the maze environment
	enviroment = kuimaze.HardMaze(map_image=MAP, probs=PROBS, grad=GRAD)

	best_policy = learn_policy(enviroment)
